﻿using Autofac;

namespace Valera.Serialization.Json
{
    public static class InfrastructureModule
    {
        public static ContainerBuilder UseJsonSerializer(this ContainerBuilder builder)
        {
            builder.RegisterType<JsonSerializer>().AsImplementedInterfaces().SingleInstance();
            return builder;
        }
    }
}
